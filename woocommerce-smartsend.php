<?php
/*
	Plugin Name: Woo Commerce - Smart Send Shipping Plugin
	Plugin URI: http://codexmedia.com.au/woocommerce-smart-send-shipping-plugin/
	Description: Add Smart Send shipping calculations to Woo Commerce e-commerce plugin
	Version: 1.4.10
	Author:  Paul Appleyard
	Author URI: http://codexmedia.com.au/
	License: GPL v2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/


require_once( 'includes/smartSendUtils.php' );
require_once('smartsend-plugin.php');
require_once('includes/smartsend-checkout.php');